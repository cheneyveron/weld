package cn.net.wangchenyu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeldApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeldApplication.class, args);
	}
}
