package cn.net.wangchenyu.controllers;

import cn.net.wangchenyu.daos.LoginRecordDao;
import cn.net.wangchenyu.daos.UserDao;
import cn.net.wangchenyu.models.LoginRecord;
import cn.net.wangchenyu.models.ReturnMessage;
import cn.net.wangchenyu.models.Users;
import cn.net.wangchenyu.services.AuthService;
import cn.net.wangchenyu.services.LoginService;
import cn.net.wangchenyu.session.SessionUser;
import cn.net.wangchenyu.util.RandomCharUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by cheneyveron on 9/7/16.
 */
@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private AuthService authService;
    @Autowired
    private LoginService loginService;


    //注册验证
    //一旦检测到已有注册用户则拒绝注册
    @RequestMapping("/register_verify")
    public Object RegisterVerify(String name, String password){
        ReturnMessage returnMessage = new ReturnMessage();
        if(authService.isAuthenticated()){
           //如果已经登录就报错
            returnMessage.id = 1;
            returnMessage.message = "您已登录";
        }else if(loginService.register(name,password)){
            returnMessage.id = 0;
            returnMessage.message = "注册成功";
        }

        return returnMessage;
    }

    //登录验证
    @RequestMapping("/login_verify")
    public Object LoginVerify(String name, String password){
        ReturnMessage returnMessage = new ReturnMessage();
        if(authService.isAuthenticated()){
            //如果已经登录就报错
            returnMessage.id = 1;
            returnMessage.message = "您已登录";
        }else if(loginService.login(name,password)){
            returnMessage.id = 0;
            returnMessage.message = "登录成功";
        }

        return returnMessage;
    }
}
