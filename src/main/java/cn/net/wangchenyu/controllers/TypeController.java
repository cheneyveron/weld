package cn.net.wangchenyu.controllers;

import cn.net.wangchenyu.daos.TypesDao;
import cn.net.wangchenyu.models.ReturnMessage;
import cn.net.wangchenyu.models.Types;
import cn.net.wangchenyu.services.AuthService;
import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cheneyveron on 9/7/16.
 * 获取、设置所有类型
 */
@RestController
@RequestMapping("/types")
public class TypeController {

    @Autowired
    private TypesDao typesDao;


    @Autowired
    private AuthService authService;

    //更新类型 添加&&修改
    @RequestMapping("/set")
    public Object SetType(Types types){
        ReturnMessage returnMessage = new ReturnMessage();
        //如果未认证就先登录
        if(!authService.isAuthenticated()){
            returnMessage.id = 1;
            returnMessage.message = "请先登录！";
        }else{
            //已经认证则直接保存数据库
            typesDao.save(types);
            returnMessage.id = 0;
            returnMessage.message = "操作成功。";
        }

        return returnMessage;
    }

    //删除类型
    @RequestMapping("/del")
    public Object DelType(int id){
        ReturnMessage returnMessage = new ReturnMessage();
        //如果未认证就先登录
        if(!authService.isAuthenticated()){
            returnMessage.id = 1;
            returnMessage.message = "请先登录！";
            return returnMessage;
        }else{
            //已经认证则删除
            typesDao.delete(id);
            returnMessage.id = 0;
            returnMessage.message = "删除成功。";
        }

        return returnMessage;
    }

    //获取类型
    @RequestMapping("/getall")
    public Object GetType(){
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.id = 0;
        returnMessage.message = typesDao.findAll();//直接返回所有
        return returnMessage;
    }
}
