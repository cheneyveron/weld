package cn.net.wangchenyu.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cheneyveron on 9/6/16.
 */
@RestController
public class IndexController {
    @RequestMapping("/")
    public String ShowIndex(){
        return "hello cheney!";
    }
}
