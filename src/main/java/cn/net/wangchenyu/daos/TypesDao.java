package cn.net.wangchenyu.daos;

import cn.net.wangchenyu.models.Types;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by cheneyveron on 9/7/16.
 */
public interface TypesDao extends CrudRepository<Types,Integer> {
    //
}
