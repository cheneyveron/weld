package cn.net.wangchenyu.daos;

import cn.net.wangchenyu.models.LoginRecord;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by cheneyveron on 9/7/16.
 */
public interface LoginRecordDao extends CrudRepository<LoginRecord,Integer> {
    //按用户编号查找用户
    List<LoginRecord> findByNoAndToken(int no,String token);
    //查找no的最新登录记录
    List<LoginRecord> findTop2ByNoOrderByNoDesc(int no);
}
