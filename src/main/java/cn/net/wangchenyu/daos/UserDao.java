package cn.net.wangchenyu.daos;

import cn.net.wangchenyu.models.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by cheneyveron on 9/7/16.
 */
public interface UserDao extends CrudRepository<Users,Integer> {
    List<Users> findByNameAndPassword(String name, String password);
    List<Users> findByName(String name);
}
