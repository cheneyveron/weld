package cn.net.wangchenyu.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by cheneyveron on 9/7/16.
 * 登录记录
 */
@Entity
public class LoginRecord {
    public LoginRecord() {
    }

    @Id

    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int no;//用户id
    private String token;//此次登录token
    private Date time;//登录时间

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public LoginRecord(int no, String token, Date time) {

        this.no = no;
        this.token = token;
        this.time = time;
    }
}
