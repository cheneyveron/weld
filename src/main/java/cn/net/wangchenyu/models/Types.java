package cn.net.wangchenyu.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by cheneyveron on 9/7/16.
 * 所有类别
 */
@Entity
public class Types {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    //排序优先级

    public Types() {
    }

    private int ord;
    private int father;//父类id
    private String name;//项目名
    private String file;//文件名
    private String other;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder() {
        return ord;
    }

    public void setOrder(int order) {
        this.ord = order;
    }

    public int getFather() {
        return father;
    }

    public void setFather(int father) {
        this.father = father;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Types(int order, int father, String name, String file, String other) {

        this.ord = order;
        this.father = father;
        this.name = name;
        this.file = file;
        this.other = other;
    }
}
