package cn.net.wangchenyu.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by cheney on 9/20/16.
 */
@Entity
public class Standard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;//名字
    private String usage;//用途
    private String source;//来源
    private String year;//年份
    private String url;//文件地址
    private int fatherid;//父类id

    public Standard(String name, String usage, String source, String year, String url, int fatherid) {
        this.name = name;
        this.usage = usage;
        this.source = source;
        this.year = year;
        this.url = url;
        this.fatherid = fatherid;
    }

    public Standard() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getFatherid() {
        return fatherid;
    }

    public void setFatherid(int fatherid) {
        this.fatherid = fatherid;
    }
}
