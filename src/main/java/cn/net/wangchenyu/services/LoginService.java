package cn.net.wangchenyu.services;

import cn.net.wangchenyu.daos.LoginRecordDao;
import cn.net.wangchenyu.daos.UserDao;
import cn.net.wangchenyu.models.LoginRecord;
import cn.net.wangchenyu.models.Users;
import cn.net.wangchenyu.session.SessionUser;
import cn.net.wangchenyu.util.RandomCharUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by cheneyveron on 9/7/16.
 */
@Service
public class LoginService {

    @Autowired
    private AuthService authService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private LoginRecordDao loginRecordDao;

    public boolean login(String name,String password){
        if (name != null && password != null) {
            List<Users> userses = userDao.findByName(name);
            if( !userses.isEmpty()){
                //非空，找到此用户
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

                if(passwordEncoder.matches(password, userses.get(0).getPassword())){
                    //登录成功
                    Users thisUsers = userses.get(0);

                    //保存登录记录
                    Date thisTime = new Date(System.currentTimeMillis());
                    String thisToken = RandomCharUtil.getRandomNumberUpperLetterChar(16);
                    LoginRecord loginRecord = new LoginRecord(thisUsers.getNo(),thisToken,thisTime);
                    loginRecordDao.save(loginRecord);

                    //设置session
                    httpSession.setAttribute("visit_time", thisTime.getTime());
                    httpSession.setAttribute("visit_token", thisToken);
                    httpSession.setAttribute("visit_user_name", thisUsers.getName());
                    httpSession.setAttribute("visit_user_id",thisUsers.getNo());
                    httpSession.setMaxInactiveInterval(1200);//20分钟不活动则会话失效

                    return true;
                }else{
                    //顺便把session失效
                    httpSession.invalidate();
                }
            }else{
                //顺便把session失效
                httpSession.invalidate();
            }
        }

        return false;
    }

    public boolean register(String name,String password) {
        if (name != null && password != null) {
            //如果数据库已经有数据报错
            Iterable<Users> theOne = userDao.findAll();
            if (theOne == null) {
                //没有数据
                Users users = new Users(name, password);
                userDao.save(users);
                return this.login(name, password);//直接登录
            }
        }

        return false;
    }
}