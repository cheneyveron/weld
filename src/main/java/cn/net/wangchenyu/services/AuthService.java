package cn.net.wangchenyu.services;

import cn.net.wangchenyu.daos.LoginRecordDao;
import cn.net.wangchenyu.models.LoginRecord;
import cn.net.wangchenyu.session.SessionUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by cheneyveron on 9/7/16.
 */
@Service
public class AuthService {

    @Autowired
    private LoginRecordDao loginRecordDao;
    @Autowired
    private HttpSession httpSession;

    public boolean isAuthenticated(){
        if(httpSession.getAttribute("visit_user_id")==null){
            return false;
        }//如果没有session信息,直接重定向到login
        else {//有id信息,查数据库对应的token相符
            List<LoginRecord> loginRecordList = loginRecordDao.findByNoAndToken((int)httpSession.getAttribute("visit_user_id"),(String)httpSession.getAttribute("visit_token"));
            if(loginRecordList.isEmpty())//如果不存在此id,清除session
            {
                httpSession.invalidate();
            }else{
                return true;
            }
        }
        return false;
    }

    public SessionUser currentUser(){
        SessionUser sessionUser = null;
        //如果已有visit_user_id就返回SessionUser对象
        if(httpSession.getAttribute("visit_user_id")!=null){
            sessionUser = new SessionUser((int)httpSession.getAttribute("visit_time"),
                    (String)httpSession.getAttribute("visit_token"),
                    (String)httpSession.getAttribute("visit_user_name"),
                    (int)httpSession.getAttribute("visit_user_id")
            );//创建新session对象
        }
        return sessionUser;
    }
}
